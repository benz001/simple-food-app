import 'dart:convert';

import 'package:sample_food_app/model/food.dart';
import 'package:http/http.dart' as http;

// Bagian dari layer controller untuk menghubungkan antara model dan view

class APIService {

  // Fungsi untuk mengambil data dari api
  List<Food> foodFromJson(String jsonData) {
    List data = json.decode(jsonData);
    return List<Food>.from(data.map((item) => Food.fromJson(item)));
  }

  Future<List<Food>> getListFood() async {
    try {
      final response = await http
        .get('http://3.15.16.42:7000/foods');
      if (response.statusCode == 200) {
        return foodFromJson(response.body);
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }
}
