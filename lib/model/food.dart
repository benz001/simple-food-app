// Bagian dari layer model untuk menggambarkan struktur model dari ap
class Food {
  String name;
  String image;
  String desc;

  Food({this.name, this.image, this.desc});

  factory Food.fromJson(Map<String, dynamic> json){
    return Food(
      name: json['name'],
      image: json['image'],
      desc: json['desc'],
    );
  }

  // @override
  // String toString() {
  //   return 'Food{name: $name, image: $image,  desc: $desc}';
  // } 
}