import 'package:flutter/cupertino.dart';
import 'package:sample_food_app/model/food.dart';

// Bagian dari layer provider untuk menyimpan state global dan menshare nya ke layer view


class FoodProvider with ChangeNotifier {
  //List of note
  Food _food = Food(name: '', image: '', desc: '');

  Food get getFood {
    return _food;
  }

  // FoodProvider() {
  //   addNewFood(0, 'name', 'cover', 'desc', 0);
  // }

  void addNewFood(String name, String image, String desc) {
    //Note object
    Food food = Food(name: name, image: image, desc: desc);
    _food = food;
    // _food.add(food);
    notifyListeners();
  }
}