import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sample_food_app/model/food.dart';
import 'package:sample_food_app/provider/food_provider.dart';
import 'package:sample_food_app/view/detail_product_screen.dart';
import 'package:sample_food_app/service/api_service.dart';

// Bagian dari layer view untuk menampilkan ui ke user
class HomeScreen extends StatefulWidget {
  static const routeName = '/homeScreen';
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var apiService = APIService();
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    apiService.getListFood().then((value) => print(value));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    // final double itemHeight = (height - kToolbarHeight - 24) / 2;
    final double itemWidth = width / 2;

    Widget _itemFood(Food item) {
      print(item.image);
      return Container(
        child: GestureDetector(
          onTap: () {
            Provider.of<FoodProvider>(context, listen: false)
                .addNewFood(item.name, item.image, item.desc);
            Navigator.pushNamed(context, DetailProductScreen.routeName);
          },
          child: Card(
            child: Column(
              children: <Widget>[
                Expanded(
                    flex: 6,
                    child: GestureDetector(
                      onTap: () {
                        Provider.of<FoodProvider>(context, listen: false)
                            .addNewFood(item.name, item.image, item.desc);
                        Navigator.pushNamed(
                            context, DetailProductScreen.routeName);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(item.image))),
                      ),
                    )),
                Expanded(
                  flex: 4,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                          context, DetailProductScreen.routeName);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      // color: Colors.red,
                      child: Text(
                        item.name,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('List Foods'),
        centerTitle: true,
      ),
      body: Container(
        width: width,
        height: height,
        color: Colors.white,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FutureBuilder(
                future: apiService.getListFood(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  List<Food> food = snapshot.data;
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                          "Something wrong with message: ${snapshot.error.toString()}"),
                    );
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    return GridView.builder(
                        itemCount: snapshot.data.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 2,
                          crossAxisSpacing: 2,
                          childAspectRatio: itemWidth / 180,
                        ),
                        itemBuilder: (context, index) {
                          Food itemFood = food[index];
                          var itemFoodDetail = itemFood;
                          return _itemFood(itemFoodDetail);
                        });
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.green,
                        // valueColor: Animation<Color.fromRGBO(177, 0, 26, 1)>,
                      ),
                    );
                  }
                })),
      ),
    );
  }
}
