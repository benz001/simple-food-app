import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sample_food_app/model/food.dart';
import 'package:sample_food_app/provider/food_provider.dart';

// Bagian dari layer view untuk menampilkan ui ke user

class DetailProductScreen extends StatefulWidget {
  static const routeName = '/detailProductScreen';
  const DetailProductScreen({Key key}) : super(key: key);

  @override
  _DetailProductScreenState createState() => _DetailProductScreenState();
}

class _DetailProductScreenState extends State<DetailProductScreen> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    // final double itemHeight = (height - kToolbarHeight - 24) / 2;
    // final double itemWidth = width / 2;

    Widget _itemFood(Food getFood) {
      return Container(
        height: height * 0.4,
        child: Card(
          child: Column(
            children: <Widget>[
              Expanded(
                  flex: 7,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(getFood.image))),
                  )),
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    alignment: Alignment.center,
                    // color: Colors.red,
                    child: Text(

                      '('+getFood.name+')',
                      style: TextStyle(fontSize: 16),
                    )),
              ),
            ],
          ),
        ),
      );
    }

    Widget _itemFoodDescription(Food getFood) {
      return Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        // height: height * 0.60,
        // color: Colors.grey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'How To :',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              textAlign: TextAlign.start,
            ),
            Text(
              getFood.desc,
              style: TextStyle(fontSize: 16, color: Colors.grey, height: 1.5),
              textAlign: TextAlign.start,
            )
          ],
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Food Detail'),
          centerTitle: true,
        ),
        body: Consumer<FoodProvider>(
          builder: (context, FoodProvider data, child) {
            print(data.getFood);
            return Container(
              color: Colors.white,
              height: height,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    _itemFood(data.getFood),
                    _itemFoodDescription(data.getFood)
                  ],
                ),
              ),
            );
          },
        ));
  }
}
